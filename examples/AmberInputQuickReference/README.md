
## Examples

To run AMBER DPRc simulations, one needs to firstly use DeePMD-kit to [train DPRc models](https://docs.deepmodeling.com/projects/deepmd/en/latest/model/dprc.html).

- [Reanalyze.mdin](Reanalyze.mdin) reanalyzes a trajectory with a different level. The results can be used to build training data.
- [NVTMD.mdin](NVTMD.mdin) performs NVT MD simulations using a DPRc model.
- [NVTExploration.mdin](NVTExploration.mdin) prints maximum force stardard deviations of several models during simulations, used for active learning cycles.
