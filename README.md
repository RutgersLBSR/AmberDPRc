# Amber DPRc

Amber DPRc, a modified version of AmberTools, has been migrated to the official Amber repository since AmberTools 24.
This repository is used for documentation only.

Docker image
============

[A Docker image](https://github.com/njzjz/docker-amberdprc/pkgs/container/amberdprc) is provided, where AmberTools was built with DeePMD-kit and xtb:

```sh
docker pull ghcr.io/njzjz/amberdprc
```

Build from source
=================

Download a prebuilt package of DeePMD-kit
-----------------------------------------

The prebuilt DeePMD-kit package is available on the [DeePMD-kit GitHub release page](https://github.com/deepmodeling/deepmd-kit/releases).
We suggest one to download the latest version:

```sh
wget https://github.com/deepmodeling/deepmd-kit/releases/latest/download/libdeepmd_c.tar.gz
tar xzf libdeepmd_c.tar.gz
export DEEPMDHOME=$(pwd)/libdeepmd_c
```

You should now see the following files:
```sh
ls \
 ${DEEPMDHOME}/lib/libdeepmd_c.so \
 ${DEEPMDHOME}/include/deepmd.hpp \
 ${DEEPMDHOME}/include/c_api.h
```

The package is compiled with specific versions of TensorFlow and CUDA on the Linux platform.
The versions may be updated over the time.
The latest information will be updated in the [DeePMD-kit documentation](https://docs.deepmodeling.com/projects/deepmd/en/master/install/install-from-c-library.html).
Go to the [DeePMD-kit release page](https://github.com/deepmodeling/deepmd-kit/releases) for other versions.
You can also compile the DeePMD-kit by yourself.

Compile AmberTools with DeePMD-kit
----------------------------------

Download [AmberTools24_rc4](https://ambermd.org/downloads/AmberTools24_rc4.tar.bz2) or newer versions from the [AmberTools website](https://ambermd.org/AmberTools.php).
Change to the amber build directory

```sh
cd /path/to/amber/build
```

Edit `run_cmake` to define USE_XTB and XTB_DIR; e.g.,
```sh
   -DUSE_DEEPMDKIT=ON
   -DCMAKE_PREFIX_PATH=${DEEPMDHOME}
```

Compile and install amber

```sh
bash ./run_cmake
make install
```

Usage
=====
To enable GPU support, one needs to load the specific version of the CUDA Toolkit and
cuDNN libraries during runtime by setting `LD_LIBRARY_PATH`.

See [examples](examples/AmberInputQuickReference/) directory for examples.

Related Documentation
=====================
- [Amber manual](https://ambermd.org/Manuals.php)
- [DeePMD-kit documentation for DPRc](https://docs.deepmodeling.com/projects/deepmd/en/master/model/dprc.html)
- [DP-GEN documentation for DPRc](https://docs.deepmodeling.com/projects/dpgen/en/latest/run/param.html#run-jdata-model-devi-engine-amber)

References
==========
We request that if you use DPRc in a publication, to please reference
as appropriate.

To cite Deep Potential - Range Correction (DPRc) models:

[1] Jinzhe Zeng, Timothy J. Giese, ̧Sölen Ekesan, Darrin M. York, Development of
Range-Corrected Deep Learning Potentials for Fast, Accurate Quantum Mechanical/molecular
Mechanical Simulations of Chemical Reactions in Solution, Journal of Chemical Theory
and Computation, 2021, 17 (11), 6993–7009. DOI: [10.1021/acs.jctc.1c00201](https://doi.org/10.1021/acs.jctc.1c00201).

To cite AMBER/DeePMD-kit interface:

[2] Wenshuo Liang, Jinzhe Zeng, Darrin M. York, Linfeng Zhang, Han Wang, Learning DeePMD-kit: A guide to building deep potential models, in Yong Wang and Ruhong Zhou (Eds.), A Practical Guide to Recent Advances in Multiscale Modeling and Simulation of Biomolecules (pp. 6-1–6-20), AIP Publishing, Melville, New York, 2023. DOI: [10.1063/9780735425279_006](https://github.com/10.1063/9780735425279_006).

To cite AMBER and other methods implemented in AMBER, please refer to [AMBER documentation](https://ambermd.org/CiteAmber.php).

To cite DeePMD-kit and other methods implemented in DeePMD-kit, please refer to [DeePMD-kit documentation](https://docs.deepmodeling.com/projects/deepmd/en/master/credits.html).

